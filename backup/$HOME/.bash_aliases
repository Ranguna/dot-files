alias ls='ls --color=auto'
alias grep='grep --color'
alias rmatessh='ssh -R 52698:localhost:52698'
alias shutodwn='shutdown'
alias shutdonw='shutdown'
alias shutodnw='shutdown'
alias shutdnow='shutdown'
alias shutdnwo='shutdown'
alias shutodnwn='shutdown'
alias shutodnwn='shutdown'

#alias sway='sway --my-next-gpu-wont-be-nvidia'

alias gttemps='find /sys/devices -type f -name "temp*_input"'

recordfs () {
	filename=${1:-"capture.mp4"}
	ffmpeg -f x11grab -video_size 2560x1440 -framerate 60 -i :0.0 -c:v libx264 -pix_fmt yuv420p -s 2560x1440 -preset ultrafast -c:a libfdk_aac -b:a 128k -threads 0 -strict normal $filename
}
# alias recordfs='ffmpeg -y -f x11grab -video_size 2560x1440 framerate 60 -i :0.0 -c:v libx264 -pix_fmt yuv420p -s 2560x1440 preset ultrafast -c:a libfdk_aac -b:a 128k -threads 0 -strict normal capture.mp4'
record () {
	filename=${1:-"capture.mp4"}	
	screelect | (read x y w h; ffmpeg -f x11grab -video_size $(($w/2*2))x$(($h/2*2)) -framerate 60 -i :0.0+"$x","$y" -c:v libx264 -pix_fmt yuv420p -s $(($w/2*2))x$(($h/2*2)) -preset ultrafast -c:a libfdk_aac -b:a 128k -threads 0 -strict normal $filename)
}
# alias record='screelect | (read x y w h; ffmpeg -y -f x11grab -video_size $(($w/2*2))x$(($h/2*2)) -framerate 60 -i :0.0+"$x","$y" -c:v libx264 -pix_fmt yuv420p -s $(($w/2*2))x$(($h/2*2)) -preset ultrafast -c:a libfdk_aac -b:a 128k -threads 0 -strict normal capture.mp4)'

alias torchnoise='noisetorch -i -s alsa_input.pci-0000_00_1f.3.analog-stereo -t 95'

git-personal () {
	git config --local user.name "Luis Pais"
	git config --local user.email "luis.pais@pm.me"
	git config --local user.signingKey 4CC761031BD10909
}

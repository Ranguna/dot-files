#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

#PS1='[\u@\h \W]\$ '
PS1='\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=3000
HISTFILESIZE=5000

if [ -f ~/.bash_aliases ]; then
	. ~/.bash_aliases
fi

# user defined functions
cdmkdir(){
	mkdir -p "$1"; cd "$1"
}

# bash completion
source /usr/share/bash-completion/bash_completion

export SSH_AUTH_SOCK="$XDG_RUNTIME_DIR/ssh-agent.socket"

# add Pulumi to the PATH
export PATH=$PATH:$HOME/.pulumi/bin
. "$HOME/.cargo/env"

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

# js completions
source <(npm completion)
source <(deno completions bash)

GTK_THEME=Adwaita:dark

export DENO_INSTALL="/home/luis/.deno"
export PATH="$DENO_INSTALL/bin:$PATH"

# fnm
export PATH=/home/luis/.fnm:$PATH
eval "`fnm env --use-on-cd`"
source <(fnm completions --shell bash)

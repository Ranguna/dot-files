mkdir -p backup

backup() {
	BACKUP_FILE=./backup/${1#/}
	FILE=${1//'$HOME'/$HOME}
	mkdir -p $(dirname $BACKUP_FILE)
	if [ -d $FILE ] || [ -f $FILE ]; then
		cp -r $FILE $BACKUP_FILE
	else
		echo "Invalid file path '$1'"
	fi
}

restore() {
	BACKUP_FILE=./backup/${1#/}
	FILE=${1//'$HOME'/$HOME}
	mkdir -p $(dirname $BACKUP_FILE)
	if [ -d $BACKUP_FILE ] || [ -f $BACKUP_FILE ]; then
		cp -r $BACKUP_FILE $FILE
	else
		echo "Invalid file path '$1'"
	fi
}

foreach_dot_file() {
	while IFS='' read -r -a files
	do
		$1 ${files[0]}
	done < map
}

case $1 in
	backup)
		foreach_dot_file backup
		echo "Backup complete."
		;;
	restore)
		foreach_dot_file restore
		echo "Restore complete."
		;;
	*)
		echo "Unknown command '$1'."
		;;
esac
